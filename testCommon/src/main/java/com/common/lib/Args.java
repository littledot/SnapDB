package com.common.lib;

/**
 *
 */
public class Args {
    public static final long SEED = 1000;
    public static final int LOAD = 1;
    public static final int STRING_KEY_LENGTH = 64;
    public static final int STRING_VAL_LENGTH = 1024;
}
