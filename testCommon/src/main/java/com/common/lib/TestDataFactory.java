package com.common.lib;

import java.security.SecureRandom;
import java.util.Random;

/**
 *
 */
public class TestDataFactory {

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private Random rng = new SecureRandom();

    public TestDataFactory(long seed) {
        rng.setSeed(seed);
    }

    public String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rng.nextInt(AB.length())));
        return sb.toString();
    }

    public String[] randomStringArray(int arraySize, int stringLen) {
        String[] data = new String[arraySize];
        for (int i = 0; i < arraySize; i++) {
            data[i] = randomString(stringLen);
        }
        return data;
    }
}
