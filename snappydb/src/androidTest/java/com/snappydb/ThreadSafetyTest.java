package com.snappydb;

import android.support.test.InstrumentationRegistry;
import android.util.Log;
import com.common.lib.Args;
import com.common.lib.TestDataFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 *
 */
public class ThreadSafetyTest {

    TestDataFactory factory;
    private DB target;
    Executor exe;

    @Before
    public void setUp() throws Exception {
        factory = new TestDataFactory(Args.SEED);
        target = DBFactory.open(InstrumentationRegistry.getTargetContext());
        exe = Executors.newFixedThreadPool(16);
    }

    @After
    public void tearDown() throws Exception {
        target.destroy();
    }

    @Test
    public void readWrite() throws Exception {
        int count = Args.LOAD;
        final CountDownLatch countDownLatch = new CountDownLatch(count * 2);
        for (int i = 0; i < count; i++) {
            exe.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        target.put("key", factory.randomString(Args.STRING_VAL_LENGTH));
                    } catch (SnappydbException e) {
                        Log.e("", "put", e);
                    }
                    countDownLatch.countDown();
                }
            });
            exe.execute(new Runnable() {
                @Override
                public void run() {
                    String val = null;
                    try {
                        val = target.get("key", String.class);
                    } catch (SnappydbException e) {
                        Log.e("", "put", e);
                    }
                    Log.wtf("", "val=" + val);
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
    }
}
