package com.snappydb.lib;

import android.support.test.InstrumentationRegistry;
import android.util.Log;
import com.common.lib.Args;
import com.common.lib.TestDataFactory;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 */
public class BenchmarkTest {

    private TestDataFactory factory;
    private DB target;

    @Before
    public void setUp() throws Exception {
        factory = new TestDataFactory(Args.SEED);
        target = DBFactory.open(InstrumentationRegistry.getTargetContext());
    }

    @After
    public void tearDown() throws Exception {
        target.destroy();
    }

    @Test
    public void benchmarkStrings() throws Exception {
        String[] key = factory.randomStringArray(Args.LOAD, Args.STRING_KEY_LENGTH);
        String[] val = factory.randomStringArray(Args.LOAD, Args.STRING_VAL_LENGTH);

        long start = System.currentTimeMillis();

        for (int i = 0; i < Args.LOAD; i++) {
            target.put(key[i], val[i]);
        }

        long time = System.currentTimeMillis() - start;
        Log.wtf("bench", "snappy=" + time);
    }
}
