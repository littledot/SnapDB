package io.snapdb;

import android.support.test.InstrumentationRegistry;
import android.util.Log;
import com.common.lib.Args;
import com.common.lib.TestDataFactory;
import com.esotericsoftware.kryo.Kryo;
import com.litl.leveldb.DB;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 *
 */
public class ThreadSafetyTest {

    TestDataFactory factory;
    DB db;
    Kryo kryo;
    SnapDB target;
    Executor exe;

    @Before
    public void setUp() throws Exception {
        factory = new TestDataFactory(Args.SEED);
        DB db = new DB(new File(InstrumentationRegistry.getTargetContext().getFilesDir(), "test"));
        Kryo kryo = new Kryo();
        target = new SnapDB(db, kryo);
        target.open();
        exe = Executors.newFixedThreadPool(16);
    }

    @After
    public void tearDown() throws Exception {
        target.destroy();
    }

    @Test
    public void readWrite() throws Exception {
        int count = Args.LOAD;
        final CountDownLatch countDownLatch = new CountDownLatch(count * 2);
        for (int i = 0; i < count; i++) {
            exe.execute(new Runnable() {
                @Override
                public void run() {
                    target.put("key", factory.randomString(Args.STRING_VAL_LENGTH));
                    countDownLatch.countDown();
                }
            });
            exe.execute(new Runnable() {
                @Override
                public void run() {
                    String val = target.get("key", String.class);
                    Log.wtf("", "val=" + val);
                    countDownLatch.countDown();
                }
            });
        }
        countDownLatch.await();
    }
}
