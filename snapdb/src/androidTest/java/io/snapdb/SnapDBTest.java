package io.snapdb;

import android.support.test.InstrumentationRegistry;
import com.esotericsoftware.kryo.Kryo;
import com.litl.leveldb.DB;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 *
 */
public class SnapDBTest {

    SnapDB db;

    @Before
    public void setUp() throws Exception {
        db = new SnapDB(new DB(new File(
                InstrumentationRegistry.getTargetContext().getFilesDir(), "tests")), new Kryo());
        db.open();
    }

    @After
    public void tearDown() throws Exception {
        db.destroy();
    }

    @Test
    public void putGetDel() throws Exception {
        ArrayList<String> in = new ArrayList<>();
        in.add("Taylor Swift");
        in.add("Maroon 5");

        db.put("key", in);
        ArrayList<String> out1 = db.get("key", ArrayList.class);
        db.del("key");
        ArrayList<String> out2 = db.get("key", ArrayList.class);

        assertThat(out1, is(in));
        assertThat(out2, nullValue());
    }

    @Test
    public void testFindKeys() throws Exception {
        String val = "val";
        db.put("b1", val);
        db.put("a2", val);
        db.put("1a1", val);
        db.put("a1", val);
        db.put("1a2", val);
        db.put("a1a", val);

        String[] data = db.findKeys("a");

        assertThat(data.length, is(3));
        assertThat(data[0], is("a1"));
        assertThat(data[1], is("a1a"));
        assertThat(data[2], is("a2"));
    }

    @Test
    public void findValues() {
        db.put("b1", "1");
        db.put("a2", "2");
        db.put("1a1", "3");
        db.put("a1", "4");
        db.put("1a2", "5");
        db.put("a1a", "6");

        String[] data = db.findVals("a", String.class);

        assertThat(data.length, is(3));
        assertThat(data[0], is("4"));
        assertThat(data[1], is("6"));
        assertThat(data[2], is("2"));
    }
}
