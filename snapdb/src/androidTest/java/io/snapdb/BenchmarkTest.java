package io.snapdb;

import android.support.test.InstrumentationRegistry;
import android.util.Log;
import com.common.lib.Args;
import com.common.lib.TestDataFactory;
import com.esotericsoftware.kryo.Kryo;
import com.litl.leveldb.DB;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

/**
 *
 */
public class BenchmarkTest {

    private TestDataFactory factory;
    private DB db;
    private Kryo kryo;
    private SnapDB target;

    @Before
    public void setUp() throws Exception {
        factory = new TestDataFactory(Args.SEED);
        db = new DB(new File(InstrumentationRegistry.getTargetContext().getFilesDir(), "db"));
        kryo = new Kryo();
        target = new SnapDB(db, kryo);
        target.open();
    }

    @After
    public void tearDown() throws Exception {
        target.destroy();
    }

    @Test
    public void benchmarkStrings() throws Exception {
        String[] key = factory.randomStringArray(Args.LOAD, Args.STRING_KEY_LENGTH);
        String[] val = factory.randomStringArray(Args.LOAD, Args.STRING_VAL_LENGTH);

        long start = System.currentTimeMillis();

        for (int i = 0; i < Args.LOAD; i++) {
            target.put(key[i], val[i]);
        }

        long time = System.currentTimeMillis() - start;
        Log.wtf("bench", "cry=" + time);
    }
}
