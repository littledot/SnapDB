package io.snapdb;

import android.support.annotation.NonNull;
import android.support.v4.util.SimpleArrayMap;
import com.esotericsoftware.kryo.Kryo;
import com.litl.leveldb.DB;
import com.litl.leveldb.WriteBatch;

import javax.inject.Inject;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;

/**
 *
 */
public class SnapDB {

    private DB mDB;
    private Cryo mCry;

    @Inject
    public SnapDB(@NonNull DB db, @NonNull Kryo kryo) {
        mDB = db;
        mCry = new Cryo(kryo);
    }

    public void open() {
        mDB.open();
    }

    public void close() {
        mDB.close();
    }

    public void destroy() {
        mDB.close();
        mDB.destroy();
    }

    public <T> T get(@NonNull String key, @NonNull Class<T> type) {
        byte[] data = mDB.get(key.getBytes());
        return data == null ? null : mCry.deserialize(data, type);
    }

    public <T> T get(@NonNull String key) {
        byte[] data = mDB.get(key.getBytes());
        if (data != null) {
            return mCry.deserialize(data);
        }
        return null;
    }

    public void put(@NonNull String key, @NonNull Object val) {
        byte[] bytes = mCry.serialize(val);
        mDB.put(key.getBytes(), bytes);
    }

    public void del(@NonNull String key) {
        mDB.delete(key.getBytes());
    }

    public String[] findKeys(@NonNull String startsWith) {
        byte[][] data = mDB.findKeys(startsWith);
        String[] arr = new String[data.length];
        for (int i = 0, l = data.length; i < l; i++) {
            arr[i] = new String(data[i]);
        }
        return arr;
    }

    public <T> T[] findVals(@NonNull String startsWith, @NonNull Class<T> type) {
        byte[][] data = mDB.findValues(startsWith);
        T[] arr = (T[]) Array.<T>newInstance(type, data.length);
        for (int i = 0, l = data.length; i < l; i++) {
            arr[i] = mCry.deserialize(data[i], type);
        }
        return arr;
    }

    public <V> void putAtomic(SimpleArrayMap<String, V> map) {
        WriteBatch op = new WriteBatch();

        for (int i = 0, len = map.size(); i < len; i++) {
            String key = map.keyAt(i);
            V val = map.valueAt(i);

            ByteBuffer keyBuf = ByteBuffer.wrap(key.getBytes());
            byte[] bytes = mCry.serialize(val);
            ByteBuffer valBuf = ByteBuffer.wrap(bytes);

            op.put(keyBuf, valBuf);
        }
        mDB.write(op);
    }

    public <T> CryIterator<T> iterator(Class<T> type) {
        return new CryIterator<>(mCry, mDB.iterator(), type);
    }
}
