#include <string.h>
#include <jni.h>

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <android/log.h>

#include "leveldbjni.h"

#include "leveldb/db.h"
#include "leveldb/write_batch.h"
#include "leveldb/comparator.h"

static jmethodID gByteBuffer_isDirectMethodID;
static jmethodID gByteBuffer_positionMethodID;
static jmethodID gByteBuffer_limitMethodID;
static jmethodID gByteBuffer_arrayMethodID;
static jclass gByteArray_Clazz;

static jlong
nativeOpen(JNIEnv* env,
           jclass clazz,
           jstring dbpath)
{
    static bool gInited;

    if (!gInited) {
        jclass byteBuffer_Clazz = env->FindClass("java/nio/ByteBuffer");
        gByteBuffer_isDirectMethodID = env->GetMethodID(byteBuffer_Clazz, "isDirect", "()Z");
        gByteBuffer_positionMethodID = env->GetMethodID(byteBuffer_Clazz, "position", "()I");
        gByteBuffer_limitMethodID = env->GetMethodID(byteBuffer_Clazz, "limit", "()I");
        gByteBuffer_arrayMethodID = env->GetMethodID(byteBuffer_Clazz, "array", "()[B");
        // http://stackoverflow.com/a/21340794/499125
        gByteArray_Clazz = static_cast<jclass>(env->NewGlobalRef(env->FindClass("[B")));
        gInited = true;
    }

    const char *path = env->GetStringUTFChars(dbpath, 0);
    LOGI("Opening database %s", path);

    leveldb::DB* db;
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options, path, &db);
    env->ReleaseStringUTFChars(dbpath, path);

    if (!status.ok()) {
        throwException(env, status);
    } else {
        LOGI("Opened database");
    }

    return reinterpret_cast<jlong>(db);
}

static void
nativeClose(JNIEnv* env,
            jclass clazz,
            jlong dbPtr)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);
    if (db) {
        delete db;
    }

    LOGI("Database closed");
}

static jbyteArray
nativeGet(JNIEnv * env,
          jclass clazz,
          jlong dbPtr,
          jlong snapshotPtr,
          jbyteArray keyObj)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);
    leveldb::ReadOptions options = leveldb::ReadOptions();
    options.snapshot = reinterpret_cast<leveldb::Snapshot*>(snapshotPtr);

    size_t keyLen = env->GetArrayLength(keyObj);
    jbyte *buffer = env->GetByteArrayElements(keyObj, NULL);
    jbyteArray result;

    leveldb::Slice key = leveldb::Slice((const char *)buffer, keyLen);
    leveldb::Iterator* iter = db->NewIterator(options);
    iter->Seek(key);
    if (iter->Valid() && key == iter->key()) {
        leveldb::Slice value = iter->value();
        size_t len = value.size();
        result = env->NewByteArray(len);
        env->SetByteArrayRegion(result, 0, len, (const jbyte *) value.data());
    } else {
        result = NULL;
    }

    env->ReleaseByteArrayElements(keyObj, buffer, JNI_ABORT);
    delete iter;

    return result;
}

static jbyteArray
nativeGetBB(JNIEnv * env,
            jclass clazz,
            jlong dbPtr,
            jlong snapshotPtr,
            jobject keyObj)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);
    leveldb::ReadOptions options = leveldb::ReadOptions();
    options.snapshot = reinterpret_cast<leveldb::Snapshot*>(snapshotPtr);

    jint keyPos = env->CallIntMethod(keyObj, gByteBuffer_positionMethodID);
    jint keyLimit = env->CallIntMethod(keyObj, gByteBuffer_limitMethodID);
    jboolean keyIsDirect = env->CallBooleanMethod(keyObj, gByteBuffer_isDirectMethodID);
    jbyteArray keyArray;
    void* key;
    if (keyIsDirect) {
        key = env->GetDirectBufferAddress(keyObj);
        keyArray = NULL;
    } else {
        keyArray = (jbyteArray) env->CallObjectMethod(keyObj, gByteBuffer_arrayMethodID);
        key = (void*) env->GetByteArrayElements(keyArray, NULL);
    }

    jbyteArray result;
    leveldb::Slice keySlice = leveldb::Slice((const char *) key + keyPos, keyLimit - keyPos);
    leveldb::Iterator* iter = db->NewIterator(options);
    iter->Seek(keySlice);
    if (iter->Valid() && keySlice == iter->key()) {
        leveldb::Slice value = iter->value();
        size_t len = value.size();
        result = env->NewByteArray(len);
        env->SetByteArrayRegion(result, 0, len, (const jbyte *) value.data());
    } else {
        result = NULL;
    }

    if (keyArray) {
        env->ReleaseByteArrayElements(keyArray, (jbyte*) key, JNI_ABORT);
    }

    delete iter;

    return result;
}

static void
nativePut(JNIEnv *env,
          jclass clazz,
          jlong dbPtr,
          jbyteArray keyObj,
          jbyteArray valObj)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);

    size_t keyLen = env->GetArrayLength(keyObj);
    jbyte *keyBuf = env->GetByteArrayElements(keyObj, NULL);

    size_t valLen = env->GetArrayLength(valObj);
    jbyte *valBuf = env->GetByteArrayElements(valObj, NULL);

    leveldb::Status status = db->Put(leveldb::WriteOptions(),
            leveldb::Slice((const char *) keyBuf, keyLen),
            leveldb::Slice((const char *) valBuf, valLen));

    env->ReleaseByteArrayElements(keyObj, keyBuf, JNI_ABORT);
    env->ReleaseByteArrayElements(valObj, valBuf, JNI_ABORT);

    if (!status.ok()) {
        throwException(env, status);
    }
}

static void
nativePutStrKey(JNIEnv *env,
          jclass clazz,
          jlong dbPtr,
          jstring keyObj,
          jbyteArray valObj)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);

    const char* key = env->GetStringUTFChars(keyObj, 0);

    size_t valLen = env->GetArrayLength(valObj);
    jbyte *valBuf = env->GetByteArrayElements(valObj, NULL);

    leveldb::Status status = db->Put(leveldb::WriteOptions(),
            key,
            leveldb::Slice((const char *) valBuf, valLen));

    env->ReleaseStringUTFChars(keyObj, key);
    env->ReleaseByteArrayElements(valObj, valBuf, JNI_ABORT);

    if (!status.ok()) {
        throwException(env, status);
    }
}

static void
nativeDelete(JNIEnv *env,
             jclass clazz,
             jlong dbPtr,
             jbyteArray keyObj)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);

    size_t keyLen = env->GetArrayLength(keyObj);
    jbyte *buffer = env->GetByteArrayElements(keyObj, NULL);

    leveldb::Status status = db->Delete(leveldb::WriteOptions(), leveldb::Slice((const char *) buffer, keyLen));
    env->ReleaseByteArrayElements(keyObj, buffer, JNI_ABORT);

    if (!status.ok()) {
        throwException(env, status);
    }
}

static void
nativeWrite(JNIEnv *env,
            jclass clazz,
            jlong dbPtr,
            jlong batchPtr)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);

    leveldb::WriteBatch *batch = (leveldb::WriteBatch *) batchPtr;
    leveldb::Status status = db->Write(leveldb::WriteOptions(), batch);
    if (!status.ok()) {
        throwException(env, status);
    }
}

leveldb::Iterator*
newIterator(JNIEnv* env,
            jclass clazz,
            jlong dbPtr,
            jlong snapshotPtr)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);
    leveldb::ReadOptions options = leveldb::ReadOptions();
    if(snapshotPtr > 0){
        options.snapshot = reinterpret_cast<leveldb::Snapshot*>(snapshotPtr);
    }
    return db->NewIterator(options);
}

static jlong
nativeIterator(JNIEnv* env,
               jclass clazz,
               jlong dbPtr,
               jlong snapshotPtr)
{
    return reinterpret_cast<jlong>(newIterator(env, clazz, dbPtr, snapshotPtr));
}

static jobjectArray
nativeFindKeys(JNIEnv *env,
               jclass clazz,
               jlong dbPtr,
               jlong snapshotPtr,
               jstring startObj)
{
    const char* startStr = env->GetStringUTFChars(startObj, 0);
    leveldb::Slice start = leveldb::Slice(startStr);
    leveldb::Iterator *it = newIterator(env, clazz, dbPtr, snapshotPtr);

    std::vector<jbyteArray> keys;
    for(it->Seek(start); it->Valid() && it->key().starts_with(start); it->Next()){
        jbyteArray key = env->NewByteArray(it->key().size());
        env->SetByteArrayRegion(key, 0, it->key().size(), (const jbyte *) it->key().data());
        keys.push_back(key);
    }

    jobjectArray keysArray = env->NewObjectArray(keys.size(), gByteArray_Clazz, 0);
    for(int i=0; i<keys.size(); i++){
        env->SetObjectArrayElement(keysArray, i, keys[i]);
    }

    env->ReleaseStringUTFChars(startObj, startStr);
    delete it;
    return keysArray;
}

static jobjectArray
nativeFindValues(JNIEnv *env,
                 jclass clazz,
                 jlong dbPtr,
                 jlong snapshotPtr,
                 jstring startObj)
{
    const char* startStr = env->GetStringUTFChars(startObj, 0);
    leveldb::Slice start = leveldb::Slice(startStr);
    leveldb::Iterator *it = newIterator(env, clazz, dbPtr, snapshotPtr);

    std::vector<jbyteArray> vals;
    for(it->Seek(start); it->Valid() && it->key().starts_with(start); it->Next()){
        jbyteArray val = env->NewByteArray(it->value().size());
        env->SetByteArrayRegion(val, 0, it->value().size(), (const jbyte *) it->value().data());
        vals.push_back(val);
    }

    jobjectArray valsArray = env->NewObjectArray(vals.size(), gByteArray_Clazz, 0);
    for(int i=0; i<vals.size(); i++){
        env->SetObjectArrayElement(valsArray, i, vals[i]);
    }

    env->ReleaseStringUTFChars(startObj, startStr);
    delete it;
    return valsArray;
}

static jlong
nativeGetSnapshot(JNIEnv *env,
                  jclass clazz,
                  jlong dbPtr)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);
    const leveldb::Snapshot* snapshot = db->GetSnapshot();
    return reinterpret_cast<jlong>(snapshot);
}

static void
nativeReleaseSnapshot(JNIEnv *env,
                      jclass clazz,
                      jlong dbPtr,
                      jlong snapshotPtr)
{
    leveldb::DB* db = reinterpret_cast<leveldb::DB*>(dbPtr);
    const leveldb::Snapshot *snapshot = reinterpret_cast<leveldb::Snapshot*>(snapshotPtr);
    db->ReleaseSnapshot(snapshot);
}

static void
nativeDestroy(JNIEnv *env,
              jclass clazz,
              jstring dbpath)
{
    const char* path = env->GetStringUTFChars(dbpath,0);
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = DestroyDB(path, options);
    if (!status.ok()) {
        throwException(env, status);
    }
}

static JNINativeMethod sMethods[] =
{
        { "nativeOpen", "(Ljava/lang/String;)J", (void*) nativeOpen },
        { "nativeClose", "(J)V", (void*) nativeClose },
        { "nativeGet", "(JJ[B)[B", (void*) nativeGet },
        { "nativeGet", "(JJLjava/nio/ByteBuffer;)[B", (void*) nativeGetBB },
        { "nativePut", "(J[B[B)V", (void*) nativePut },
        { "nativePut", "(JLjava/lang/String;[B)V", (void*) nativePutStrKey },
        { "nativeDelete", "(J[B)V", (void*) nativeDelete },
        { "nativeWrite", "(JJ)V", (void*) nativeWrite },
        { "nativeIterator", "(JJ)J", (void*) nativeIterator },
        { "nativeFindKeys", "(JJLjava/lang/String;)[[B", (void*) nativeFindKeys },
        { "nativeFindValues", "(JJLjava/lang/String;)[[B", (void*) nativeFindValues },
        { "nativeGetSnapshot", "(J)J", (void*) nativeGetSnapshot },
        { "nativeReleaseSnapshot", "(JJ)V", (void*) nativeReleaseSnapshot },
        { "nativeDestroy", "(Ljava/lang/String;)V", (void*) nativeDestroy }
};

int
register_com_litl_leveldb_DB(JNIEnv *env) {
    jclass clazz = env->FindClass("com/litl/leveldb/DB");
    if (!clazz) {
        LOGE("Can't find class com.litl.leveldb.DB");
        return 0;
    }

    return env->RegisterNatives(clazz, sMethods, NELEM(sMethods));
}
